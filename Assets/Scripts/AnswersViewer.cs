using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AnswersViewer : MonoBehaviour
{
    [SerializeField] protected TaskNames taskName;

    [SerializeField] protected InputField inputField;

    [SerializeField] protected GameObject evaluationBlock;

    [SerializeField] protected Formatting formatting;
    [SerializeField] protected DialogBox dialogBox;
    [SerializeField] protected CanvasScaler canvasScaler;

    [SerializeField] protected GameObject loadingOverlay;

    [SerializeField] protected Text selectedCourseView;
    [SerializeField] protected VariantsStorage courseVariantsStorage;
    [SerializeField] protected GameObject coursesScrollView;

    [SerializeField] protected InputProvider groupInputProvider;

    [SerializeField] protected Text selectedFIOView;
    [SerializeField] protected VariantsStorage fIOVariantsStorage;
    [SerializeField] protected GameObject fIOScrollView;

    [SerializeField] protected VariantsStorage userAllAnswersStorage;

    [SerializeField] protected AnswerElement answerElement;
    [SerializeField] protected Transform answersParent;

    protected ServerCommunication serverCommunication = new ServerCommunication();

    protected List<AnswerElement> answerElements = new List<AnswerElement>();
    protected List<string> userAnswers = new List<string>();
    
    protected int answerIDIndex;
    protected string currentAnswerID;
    protected string[] answersLines;
    protected string[] evaluationsLines;
    
    private List<User> usersOfGroup = new List<User>();
    private List<GameObject> fIOSelectors = new List<GameObject>();
    private List<GameObject> userAnswersTemporaryStorage = new List<GameObject>();
   
    private int dBUserIDIndex;
    private int lastNameIndex;
    private int firstNameIndex;
    private int patronymicIndex;
    private int evaluationIndex;
    private string indicator;
    private string currentUserID;
    private string userDBRawData;
    private string[] userDBLines;
    private string[] tittlesUDB;
    private string[] indicatorUDB;

    private readonly string tittleUDBTag = "#TittleUDB";
    private readonly string indicatorUDBTag = "#IndicatorUDB";



    private void Start()
    {
        courseVariantsStorage.OnSelected += OnSelectedCourse;
        groupInputProvider.OnPressedButton += FindUsersByGroupNumber;
        fIOVariantsStorage.OnSelected += OnSelectedUser;
        userAllAnswersStorage.OnSelected += OnUserAnswerSelected;

        LoadUsersDBsList();
    }

    public void RedrawCanvas()
    {
        CanvasRedrawer.Redraw(canvasScaler);
    }

    public void ShowUsersList()
    {
        fIOScrollView.SetActive(true);

        RedrawCanvas();
    }

    public void AddExpertEvaluation()
    {
        loadingOverlay.SetActive(true);

        string evaluation = $"{currentAnswerID}~{currentUserID}~{inputField.text}~";

        serverCommunication.AddExpertEvaluation(indicator, taskName.ToString(), evaluation, GetSendingResult);
    }

    private void GetSendingResult(bool isSent, string serverResponse)
    {
        loadingOverlay.SetActive(false);

        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = "������: ",
            AcceptBtnText = "��",
            WithoutRejectBtn = true
        };

        if (isSent)
        {
            config.Message += "����������";

            inputField.Select();
            inputField.text = "";
        }
        else
        {
            config.Message += serverResponse;
            config.OnPressedBtnCallback = null;
        }

        dialogBox.Show(config);
    }

    private async void LoadUsersDBsList()
    {
        loadingOverlay.SetActive(true);

        string[] lines = formatting.SplitOnLines(await serverCommunication.GetUsersDBsList());

        tittlesUDB = formatting.GetCellsValues(formatting.GetSubstringAfterTag(tittleUDBTag, lines));
        indicatorUDB = formatting.GetCellsValues(formatting.GetSubstringAfterTag(indicatorUDBTag, lines));

        CreateCourseSelectors();
    }

    private void CreateCourseSelectors()
    {
        courseVariantsStorage.SetCreatingValues(tittlesUDB);

        loadingOverlay.SetActive(false);
    }

    private async void OnSelectedCourse(int index)
    {
        loadingOverlay.SetActive(true);

        selectedCourseView.text = tittlesUDB[index];

        indicator = indicatorUDB[index];

        userDBRawData = await serverCommunication.GetUsersDB(indicator);
        userDBLines = formatting.SplitOnLines(userDBRawData);

        answersLines = formatting.SplitOnLines(await serverCommunication.GetAllUsersAnswers(indicator, taskName.ToString()));
        evaluationsLines = formatting.SplitOnLines(await serverCommunication.GetEvaluations(indicator, taskName.ToString()));

        SetIndexes();

        SetTittlesForAnswerElements();

        coursesScrollView.SetActive(false);

        RedrawCanvas();
    }

    protected virtual void SetIndexes()
    {
        string userDataMask = userDBLines[0];

        dBUserIDIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.UserID}", userDataMask);
        lastNameIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.LastName}", userDataMask);
        firstNameIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.FirstName}", userDataMask);
        patronymicIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.Patronymic}", userDataMask);

        string answerMask = answersLines[0];

        answerIDIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.AnswerID}", answerMask);

        string evaluaitionMask = evaluationsLines[0];

        evaluationIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.Evaluation}", evaluaitionMask);
    }

    protected abstract void SetTittlesForAnswerElements();

    protected void CreateAnswerElements(string[] tittlesForAnswers)
    {
        for (int i = 0; i < answerElements.Count; i++)
        {
            Destroy(answerElements[i].gameObject);
        }

        answerElements.Clear();

        for (int i = 0; i < tittlesForAnswers.Length; i++)
        {
            AnswerElement element = Instantiate(answerElement, answersParent);

            element.SetTittle(tittlesForAnswers[i]);

            answerElements.Add(element);
        }

        CreateEvaluationField();
    }

    private void CreateEvaluationField()
    {
        AnswerElement element = Instantiate(answerElement, answersParent);

        element.SetTittle("���������� ������");

        answerElements.Add(element);

        loadingOverlay.SetActive(false);
    }

    private void FindUsersByGroupNumber(string number)
    {
        string groupBeginIndicator = $"#{number}Begin";
        string groupEndIndicator = $"#{number}End";

        int lineStart = -1;
        int lineEnd = -1;

        for (int i = 0; i < userDBLines.Length; i++)
        {
            if (userDBLines[i].StartsWith(groupEndIndicator))
            {
                lineEnd = i;
                break;
            }
            else if (userDBLines[i].StartsWith(groupBeginIndicator))
            {
                lineStart = i + 1;
            }
        }

        if (lineStart == -1)
        {
            DialogBoxConfig config = new DialogBoxConfig()
            {
                Message = "������ ������ �� ������",
                AcceptBtnText = "��",
                WithoutRejectBtn = true
            };

            dialogBox.Show(config);

            return;
        }

        List<string> usersLines = new List<string>();

        int usersCount = lineEnd - lineStart;

        for (int i = 0; i < usersCount; i++)
        {
            usersLines.Add(userDBLines[lineStart + i]);
        }

        usersOfGroup.Clear();

        for (int i = 0; i < usersCount; i++)
        {
            string[] cellsValues = formatting.GetCellsValues(usersLines[i]);

            User user = new User()
            {
                UserID = cellsValues[dBUserIDIndex],
                LastName = cellsValues[lastNameIndex],
                FirstName = cellsValues[firstNameIndex],
                Patronymic = cellsValues[patronymicIndex],
                GroupNumber = number
            };

            usersOfGroup.Add(user);
        }

        string[] fIOUsersOfGroup = new string[usersOfGroup.Count];

        for (int i = 0; i < usersOfGroup.Count; i++)
        {
            User user = usersOfGroup[i];
            fIOUsersOfGroup[i] = $"{user.LastName} {user.FirstName} {user.Patronymic}";
        }

        for (int i = 0; i < fIOSelectors.Count; i++)
        {
            Destroy(fIOSelectors[i]);
        }

        fIOSelectors.Clear();

        fIOVariantsStorage.SetCreatingValues(fIOUsersOfGroup, AddFIOSelector);

        RedrawCanvas();

        DialogBoxConfig goodLoading = new DialogBoxConfig()
        {
            Message = "������ ������ ��������",
            AcceptBtnText = "��",
            WithoutRejectBtn = true
        };

        dialogBox.Show(goodLoading);
    }

    private void AddFIOSelector(GameObject selector)
    {
        fIOSelectors.Add(selector);
    }

    private void OnSelectedUser(int index)
    {
        loadingOverlay.SetActive(true);

        fIOScrollView.SetActive(false);

        User user = usersOfGroup[index];

        currentUserID = user.UserID;

        selectedFIOView.text = $"{user.LastName} {user.FirstName} {user.Patronymic}";

        ShowAllUserAnswers();

        RedrawCanvas();
    }

    private void ShowAllUserAnswers()
    {
        userAnswers.Clear();

        for (int i = 0; i < answersLines.Length; i++)
        {
            if (answersLines[i].IndexOf(currentUserID) != -1)
            {
                userAnswers.Add(answersLines[i]);
            }
        }

        List<string> selectorsTittles = new List<string>();
        
        for (int i = 0; i < userAnswers.Count; i++)
        {
            selectorsTittles.Add(formatting.GetCellsValues(userAnswers[i])[0]);
        }

        for (int i = 0; i < userAnswersTemporaryStorage.Count; i++)
        {
            Destroy(userAnswersTemporaryStorage[i]);
        }

        userAnswersTemporaryStorage.Clear();

        userAllAnswersStorage.SetCreatingValues(selectorsTittles.ToArray(), OnUserAnswerCreated);

        loadingOverlay.SetActive(false);
    }

    protected abstract void OnUserAnswerSelected(int index);

    protected string CheckEvaluationFilling()
    {
        string evaluation = "";

        string id = $"{currentAnswerID}~";

        for (int i = 0; i < evaluationsLines.Length; i++)
        {
            if (evaluationsLines[i].IndexOf(id) != -1)
            {
                evaluation = formatting.GetCellsValues(evaluationsLines[i])[evaluationIndex];

                break;
            }
        }

        if (evaluation == "")
        {
            evaluationBlock.SetActive(true);
        }
        else 
        {
            evaluationBlock.SetActive(false);
        }

        return evaluation;
    }

    private void OnUserAnswerCreated(GameObject userAnswer)
    {
        userAnswersTemporaryStorage.Add(userAnswer);
    }
}