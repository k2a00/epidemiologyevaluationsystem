public class NoticeAnswersViewer : AnswersViewer
{
    private int variantIndex;
    private int setOfVariantsIdIndex;



    protected async override void OnUserAnswerSelected(int index)
    {
        string[] answerCellValues = formatting.GetCellsValues(userAnswers[index]);

        currentAnswerID = answerCellValues[answerIDIndex];

        string setOfVariantsID = answerCellValues[setOfVariantsIdIndex];

        string[] variantInfo = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.SetOfVariants}-{setOfVariantsID}",
            await serverCommunication.GetSetOfVariantsByID(setOfVariantsID)));


        int answerElementsCount = answerElements.Count;

        for (int a = 0; a < answerElementsCount - 1; a++)
        {
            if (a == variantIndex)
            {
                answerElements[a].SetAnswer(variantInfo[int.Parse(answerCellValues[a])]);

                continue;
            }

            answerElements[a].SetAnswer(formatting.CheckLineBreaks(answerCellValues[a]));
        }

        int evaluationFieldIndex = answerElementsCount - 1;

        answerElements[evaluationFieldIndex].SetAnswer(CheckEvaluationFilling());

        CanvasRedrawer.Redraw(canvasScaler);
    }

    protected override void SetTittlesForAnswerElements()
    {
        string answerMask = answersLines[0];
        int endIndex = answerMask.IndexOf($"#{DataTags.UserID}") - 1;

        string[] tittlesForAnswers = formatting.GetCellsValues(answerMask.Substring(0, endIndex));

        string variantTag = $"#{DataTags.Variant}";

        for (int i = 0; i < tittlesForAnswers.Length; i++)
        {
            if (tittlesForAnswers[i] == variantTag)
            {
                tittlesForAnswers[i] = "������� � ��������";

                variantIndex = i;

                break;
            }
        }

        CreateAnswerElements(tittlesForAnswers);
    }

    protected override void SetIndexes()
    {
        base.SetIndexes();

        string answerMask = answersLines[0];

        setOfVariantsIdIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.SetOfVariantsID}", answerMask);
    }
}