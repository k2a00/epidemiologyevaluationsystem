using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesLoader : MonoBehaviour
{
    public enum Scenes { MainMenu, Notice, KitchenViolationsRoom }


    public void LoadScene(Scenes scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }
}