using System;
using UnityEngine;
using UnityEngine.UI;


public class VariantSelector : MonoBehaviour
{
    [SerializeField] private Text view;

    public event Action<int> OnSelectVariant;

    private int variantNumber;


    public void SetValues(int index, string tittle)
    {
        variantNumber = index;

        view.text = tittle;
    }

    public void Select()
    {
        OnSelectVariant?.Invoke(variantNumber);
    }
}
