using UnityEngine;

public class ViolationsRoomAnswersViewer : AnswersViewer
{
    [SerializeField] private RoomNames roomName;

    private int userAnswerIndex;
    private int violationsIDIndex;
    private int timeMarkIndex;
    private string[] violationsID;
    private string[] violationsSummary;



    protected override void OnUserAnswerSelected(int index)
    {
        string[] answerCellValues = formatting.GetCellsValues(userAnswers[index]);

        currentAnswerID = answerCellValues[answerIDIndex];

        answerElements[0].SetAnswer(answerCellValues[timeMarkIndex]);

        string[] currentViolationsID = answerCellValues[violationsIDIndex].Split('|');

        string summary = "";

        int orderNumber = 1;

        for (int i = 0, length = currentViolationsID.Length; i < length; i++)
        {
            for (int a = 0, violationsIDLength = violationsID.Length; a < violationsIDLength; a++)
            {
                if (currentViolationsID[i] == violationsID[a])
                {
                    summary += $"{orderNumber}) {violationsSummary[a]}. \n";

                    orderNumber++;

                    break;
                }
            }
        }

        answerElements[1].SetAnswer(summary);
        answerElements[2].SetAnswer(formatting.CheckLineBreaks(answerCellValues[userAnswerIndex]));
        answerElements[3].SetAnswer(CheckEvaluationFilling());

        CanvasRedrawer.Redraw(canvasScaler);
    }

    protected override void SetTittlesForAnswerElements()
    {
        string[] tittlesForAnswers = new string[] { "��������� �������", "�������� ���������", "�����" };

        CreateAnswerElements(tittlesForAnswers);
    }

    protected override async void SetIndexes()
    {
        base.SetIndexes();

        string answerMask = answersLines[0];

        timeMarkIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.TimeMark}", answerMask);
        userAnswerIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.UserAnswer}", answerMask);
        violationsIDIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.ViolationsID}", answerMask);

        string configRawData = await serverCommunication.GetViolationsRoomConfig(roomName.ToString());

        violationsID = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.ViolationsID}", configRawData));
        violationsSummary = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.ViolationsSummary}", configRawData));
    }
}