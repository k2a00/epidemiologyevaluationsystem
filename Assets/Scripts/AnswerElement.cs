using UnityEngine;
using UnityEngine.UI;

public class AnswerElement : MonoBehaviour
{
    [SerializeField] private Text tittle;
    [SerializeField] private Text answer;


    public void SetTittle(string _tittle)
    {
        tittle.text = _tittle;
    } 
    
    public void SetAnswer(string _answer)
    {
        answer.text = _answer;
    }
}
