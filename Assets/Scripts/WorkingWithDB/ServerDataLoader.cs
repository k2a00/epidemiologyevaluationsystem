using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class ServerDataLoader
{
    private bool _debug = true;


    public async Task<string> DownloadByURL(string url)
    {   
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            request.SendWebRequest();

            while (!request.isDone)
            {
                await Task.Yield();
            }


            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError ||
                request.result == UnityWebRequest.Result.DataProcessingError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                if (_debug)
                {
                    Debug.Log("Successful download");
                    Debug.Log(request.downloadHandler.text);
                }

                return request.downloadHandler.text;
            }
        }

        return "";
    }
}