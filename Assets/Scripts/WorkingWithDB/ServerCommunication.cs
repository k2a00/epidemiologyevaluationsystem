using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerCommunication
{
    private readonly string indexURL = "http://f0667571.xsph.ru/Epidemiology/";
    private readonly string serverSuccesfullResponse = "Successfully";

    private enum phpKeyWords
    {
        Function,
        UserID, 
        PrefixUserID,
        SetOfVariantsID,
        UserAnswer,
        AnswerID,
        Evaluation,
        Indicator,
        TaskName,
        RoomName
    }

    private enum phpFunctions
    {
        GetUserNoticeAnswers,
        AddNoticeAnswer, 
        GetSetOfVariantsByID,
        GetUsersDBsList,
        AddExpertEvaluation,
        CheckUserID,
        GetUsersDB,
        GetAllUsersAnswers,
        GetViolationsRoomConfig,
        GetEvaluations
    }

    public async Task<string> GetUsersDBsList()
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetUsersDBsList.ToString());

        return await SendRequest(form);
    }

    public async Task<string> GetUsersDB(string indicator)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetUsersDB.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), indicator);

        return await SendRequest(form);
    }

    public async Task<string> GetEvaluations(string indicator, string taskName)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetEvaluations.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), indicator);
        form.AddField(phpKeyWords.TaskName.ToString(), taskName);

        return await SendRequest(form);
    }

    public async Task<string> GetAllUsersAnswers(string indicator, string taskName)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetAllUsersAnswers.ToString());
        form.AddField(phpKeyWords.TaskName.ToString(), taskName);
        form.AddField(phpKeyWords.Indicator.ToString(), indicator);

        return await SendRequest(form);
    }

    public async Task<string> GetViolationsRoomConfig(string roomName)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetViolationsRoomConfig.ToString());
        form.AddField(phpKeyWords.RoomName.ToString(), roomName);

        return await SendRequest(form);
    }

    public async Task<string> GetSetOfVariantsByID(string setID)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetSetOfVariantsByID.ToString());
        form.AddField(phpKeyWords.SetOfVariantsID.ToString(), setID);

        return await SendRequest(form);
    }

    public async void AddExpertEvaluation(string indicator, string taskName, string evaluation, Action<bool, string> callback)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.AddExpertEvaluation.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), indicator);
        form.AddField(phpKeyWords.TaskName.ToString(), taskName);
        form.AddField(phpKeyWords.Evaluation.ToString(), evaluation);

        string serverResponse = await SendRequest(form);

        if (serverResponse == serverSuccesfullResponse)
        {
            callback?.Invoke(true, "");
        }
        else
        {
            callback?.Invoke(false, serverResponse);
        }
    }

    private async Task<string> SendRequest(WWWForm form)
    {
        UnityWebRequest request = UnityWebRequest.Post(indexURL, form);

        request.SendWebRequest();

        while (!request.isDone)
        {
            await Task.Yield();
        }

        return request.downloadHandler.text;
    }
}
