using UnityEngine;

public class Formatting : MonoBehaviour
{
    private const char cellSeporator = '~';
    private readonly string[] endIndicator = { "~#end" };



    public string[] SplitOnLines(string rawData)
    {
        string[] lines = rawData.Split(endIndicator, System.StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < lines.Length; i++)
        {
            lines[i] = lines[i].Trim();
        }

        return lines;
    }

    public string GetLineThatStartWith(string startingWith, string[] lines)
    {
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].StartsWith(startingWith))
            {
                return lines[i];
            }
        }

        return "";
    }

    public string[] GetCellsValues(string line)
    {
        string[] cellsValues = line.Split(cellSeporator);

        for (int i = 0; i < cellsValues.Length; i++)
        {
            cellsValues[i] = cellsValues[i];
        }

        return  cellsValues;
    }

    public int GetCellIndexThatBeginWith(string tag, string line)
    {
        string[] cellsValues = line.Split(cellSeporator);

        int neededIndex = -1;

        for (int i = 0; i < cellsValues.Length; i++)
        {
            if (cellsValues[i].StartsWith(tag))
            {
                neededIndex = i;
                break;
            }
        }

        return neededIndex;
    }

    public string GetSubstringAfterTag(string tag, string[] rawData)
    {
        string neededLine = GetLineThatStartWith(tag, rawData);
        int indexBegining = neededLine.IndexOf(cellSeporator) + 1;

        return neededLine.Substring(indexBegining).Trim();
    }

    public string GetSubstringAfterTag(string tag, string rawData)
    {
        return GetSubstringAfterTag(tag, SplitOnLines(rawData));
    }

    public string GetSubstringAfterTagWithReplacing(string tag, string[] rawData)
    {
        return GetSubstringAfterTag(tag, rawData);
    }

    public string FormatByServerRules(string value)
    {
        value = value.Replace('~', ' ');
        value = value.Replace('|', ' ');
        value = value.Replace("\n", "*n*");

        return value;
    }

    public string CheckLineBreaks(string value)
    {
        return value.Replace("*n*", "\n");
    }
}